import bpy

# Globals
AUDIO_CLIP = "/home/ash/Projects/Blender/mm2.mp3"
AUDIO_FRAMES = 5047
NUM_BARS = 50
RANGE_MIN = 20 # Lowest humans can hear
RANGE_MAX = 20000 # Highest humans can hear
BASE_FREQ = (RANGE_MAX / RANGE_MIN) ** (1 / (NUM_BARS - 1))

# Set to cycles render
bpy.context.scene.render.engine = "CYCLES"

# Add audio
bpy.context.area.type = "SEQUENCE_EDITOR"
bpy.ops.sequencer.sound_strip_add(filepath=AUDIO_CLIP, files=[{"name":"mm2.mp3", "name":"mm2.mp3"}], relative_path=True, frame_start=1, channel=1)

# Set end frame
bpy.context.scene.frame_end = AUDIO_FRAMES

# Variables for use inside the loop
l = 1
h = 1
# Iterate through however many bars you want
for i in range(0, NUM_BARS):
    # Add a plane and set it's origin to one of its edges
    bpy.ops.mesh.primitive_cube_add(location = ((i + (i*0.5)), 0, 0))
    bpy.context.scene.cursor_location = bpy.context.active_object.location
    bpy.context.scene.cursor_location.z -= 1
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    # Scale the cube, then apply the transformation
    bpy.context.active_object.scale.x = 0.5
    bpy.context.active_object.scale.y = 0.5
    bpy.context.active_object.scale.z = 20
    bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)

    # Insert a scaling keyframe and lock the x and y axis
    bpy.ops.anim.keyframe_insert_menu(type="Scaling")
    bpy.context.active_object.animation_data.action.fcurves[0].lock = True
    bpy.context.active_object.animation_data.action.fcurves[1].lock = True

    # Create a new material with nodes and link it to the object
    material = bpy.data.materials.new("Material." + str(i))
    material.use_nodes = True
    bpy.context.active_object.data.materials.append(material)

    # Get default diffuse node
    diffuse_node = material.node_tree.nodes.get("Diffuse BSDF")

    # Make a color ramp from green to red
    color_ramp_node = material.node_tree.nodes.new("ShaderNodeValToRGB")
    color_ramp_node.color_ramp.color_mode = "HSV"
    color_ramp_node.color_ramp.elements[0].color = (0, 1, 0, 1)
    color_ramp_node.color_ramp.elements[1].color = (1, 0, 0, 1)

    # Link color ramp to diffuse node
    material.node_tree.links.new(diffuse_node.inputs[0], color_ramp_node.outputs[0])

    # Set a keyframe for the color ramp factor
    color_ramp_node.inputs[0].keyframe_insert(data_path="default_value", frame=1)

    # Set the window context to the graph editor
    bpy.context.area.type = "GRAPH_EDITOR"

    # Expression to determine the frequency range of the bars
    l = h
    h = RANGE_MIN * BASE_FREQ ** i

    # Bake that range of frequencies to the current plane (along the y axis)
    bpy.ops.graph.sound_bake(filepath=AUDIO_CLIP, low = (l), high = (h))

    # Lock the y axis
    bpy.context.active_object.animation_data.action.fcurves[2].lock = True

    print("Created bar #" + str(i))

# Go back to text editor mode
bpy.context.area.type = "TEXT_EDITOR"
